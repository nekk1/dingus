from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from dingus import settings

admin.autodiscover()

urlpatterns = patterns('',	
	# Examples:
	# url(r'^$', 'dingus.views.home', name='home'),
	# url(r'^dingus/', include('dingus.foo.urls')),

	url(r'^', include('store.urls')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	url(r'^admin/', include(admin.site.urls)),
	url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
) 
urlpatterns += staticfiles_urlpatterns()
