var IndexFunctionality = {
	init: function(){
		$('.slider-index').flexslider({
			animation: "slide",
			controlsContainer: ".flex-container",
			before: IndexFunctionality.setGameDetailsText
	    });
		$('.slider-game').flexslider({
			animation: "slide",
			controlsContainer: ".flex-container",
	    });
		$('.tabs a').on('click', IndexFunctionality.changeTab);
	},

	setGameDetailsText: function(slider){
		var newSlide = $(slider.slides[slider.animatingTo]);

		$('div.previewName')
			.fadeOut(300, function(){
				$(this)
					.text(newSlide.find('span.gamename').text())
					.fadeIn(300);
			});

		$('div.previewDescription')
			.fadeOut(300, function(){
				$(this)
					.text(newSlide.find('span.gamedescription').text())
					.fadeIn(300);
			});

		$('div.previewCategory')
			.fadeOut(300, function(){
				$(this)
					.text(newSlide.find('span.gameCategory').text())
					.fadeIn(300);
			});

		$('div.previewPlatform div.platforms')
			.fadeOut(300, function(){
				$(this)
					.html(newSlide.find('div.gameplatforms').html())
					.fadeIn(300);
			});
	},

	changeTab: function(e){
		e.preventDefault();
		$('div.tabs .active').removeClass('active');
		$(this).find('>div').addClass('active');
		var tab = $(this).data('tab');
		$('.tabContent:visible').fadeOut(150, function(){
			$('.tabContent[data-tab="'+tab+'"]').fadeIn(150);
		});
	}
}

$(function(){
	IndexFunctionality.init();
});