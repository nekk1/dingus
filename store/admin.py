from django.contrib import admin
from store.models import *

admin.site.register(Platform)
admin.site.register(Publisher)
admin.site.register(Developer)
admin.site.register(Game)
admin.site.register(GameRating)
admin.site.register(GameComment)
admin.site.register(Giveaway)
admin.site.register(GameMedia)
admin.site.register(Genre)
admin.site.register(Featured)
admin.site.register(MediaType)
admin.site.register(GameForum)
admin.site.register(Category)
admin.site.register(Thread)
admin.site.register(Post)
admin.site.register(Storeuser)