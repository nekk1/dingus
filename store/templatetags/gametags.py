from django import template 
from django.db import connection
from store.models import *

register = template.Library()

@register.tag
def userowns(parser, token):
    try:
        args = token.split_contents()
        tag_name, username, game = args[0], args[1], args[2]
    except IndexError:
        m = ("'%s' tag requires at least two arguments"
             % token.contents.split()[0])
        raise TemplateSyntaxError, m

    return LoggedUserNode(username, game)


class LoggedUserNode(template.Node):

	def __init__(self, username, game):
		self.game = template.Variable(game)
		self.username = template.Variable(username)

	def render(self, context):
		game = ""
		username = ""

		try:
			username = self.username.resolve(context)
			game = self.game.resolve(context)
		except:
			game = self.game.resolve(context)
			
			if (game.price <= 0):
				return '<div class="platforms"><a href="/download/' +game.name+ '"><span title="Download" class="download"></span></a>'
			return """<div class="platforms"><a href="/game/"""+game.name+""""><span title="Add to cart" class="buy"></span></a>"""

		if (game.price <= 0):
			return '<div class="platforms"><a href="/download/' +game.name+ '"><span title="Download" class="download"></span></a>'

		if (self.username == None):
			return """<div class="platforms"><a href="/game/"""+game.name+""""><span title="Add to cart" class="buy"></span></a>"""
		try:
			username = self.username.resolve(context)
			game = self.game.resolve(context)
		except:
			game = self.game.resolve(context)
			return """<div class="platforms"><a href="/game/"""+game.name+""""><span title="Add to cart" class="buy"></span></a>"""

		userID = Storeuser.objects.get(username=username).id

		cursor = connection.cursor()
		cursor.execute("Select id from STORE_STOREUSER_GAMESOWNED where game_id = %s and storeuser_id = %s", [game.id, userID])

		if (cursor.fetchone() != None):
			return '<div class="platforms"><a href="/download/' +game.name+ '"><span title="Download" class="download"></span></a>'
		return '<div class="platforms"><a href="/game/' +game.name+ '/buy"><span title="Add to cart" class="buy"></span></a>'