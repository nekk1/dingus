from django.conf.urls import patterns, url
from store import views

urlpatterns = patterns('',
	url(r'^games/newarrivals/$', views.gamesNewArrivals, name='newarrivals'),
	url(r'^games/genre/(?P<genre>.*)$', views.gamesGenre, name='genre'),
	url(r'^games/cheapest/$', views.cheapestGames, name='cheapest'),
	url(r'^games/publisher/(?P<publisher>.*)$', views.gamesByPublisher, name='gamesbypublisher'),
	url(r'^games/$', views.gamesRoot, name='gamesroot'),

	url(r'^game/(?P<game_name>.*)/addComment$', views.addComment, name='addComment'),
	url(r'^game/(?P<game_name>.*)/addRating$', views.addRating, name='addRating'),
	

	#buying game
	url(r'^game/(?P<game_name>.*)/buy$', views.cartRedirect, name='cartRedirect'),
	url(r'^game/(?P<game_name>.*)/buy/confirm$', views.addToCart, name='addToCart'),

	url(r'^game/(?P<game_name>.*)$', views.game, name='game'),

	#cart
	url(r'^cart$', views.cart, name='cart'),
	url(r'^cart/buyGames$', views.buyGames, name='buyGamesFromCart'),
	url(r'^cart/buyGames/confirm$', views.buyGamesConfirm, name='buyGamesFromCartConfirm'),
	url(r'^cart/(?P<game_name>.*)/remove$', views.removeFromCart, name='removeFromCart'),
	url(r'^clearCart$', views.clearCart, name='clearCart'),

	#other
	url(r'^download/(?P<game_name>.*)$', views.downloadGame, name='download'),

	#Users
	url(r'^login$', views.login, name='login'),
	url(r'^logout$', views.logout, name='logout'),
	url(r'^verify$', views.verify, name='verify'),
	url(r'^user/addCash$', views.addCash, name='addCash'),
	url(r'^user/(?P<user_id>\d+)$', views.user, name='user'),

	#Forums
	url(r'^forum/createPost/(?P<thread_id>.*)$', views.createPost, name='createPost'),
	url(r'^forum/deletePost/(?P<post_id>.*)$', views.deletePost, name='deletePost'),
	url(r'^forum/createThread/(?P<category_id>.*)$', views.createThread, name='createThread'),
	url(r'^forum/deleteThread/(?P<thread_id>.*)$', views.deleteThread, name='deleteThread'),
	url(r'^forum/createCategory/(?P<forum_id>.*)$', views.createCategory, name='createCategory'),
	url(r'^forum/deleteCategory/(?P<category_id>.*)$', views.deleteCategory, name='deleteCategory'),
	url(r'^forum/thread/(?P<thread_id>.*)/(?P<page>\d+)$', views.thread, name='thread'),
	url(r'^forum/(?P<game_name>.*)/(?P<category>.*)$', views.forumCategory, name='forumCategory'),
	url(r'^forum/(?P<game_name>.*)$', views.forum, name='forum'),

	#Developers
	url(r'^devs/$', views.devs, name='devs'),
	url(r'^dev/(?P<dev_name>.*)$', views.dev, name='devname'),
	url(r'^dev/$', views.dev, name='dev'),
	url(r'^games/dev/$', views.devgames, name='devgames'),

	#Publishers
	url(r'^publisher/(?P<pub_name>.*)$', views.publisher, name='publisher'),
	url(r'^publisher/$', views.publisher, name='publisher'),
	url(r'^publishers/$', views.publishers, name='publishers'),
	url(r'^games/publisher/$', views.publisher, name='publishergames'),

	#Stats
	url(r'^stats/', views.stats, name='stats'),

	#index
	url(r'^$', views.index, name='store_index')
)