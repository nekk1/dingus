# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Publisher'
        db.create_table('store_publisher', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal('store', ['Publisher'])

        # Adding model 'Developer'
        db.create_table('store_developer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal('store', ['Developer'])

        # Adding model 'Game'
        db.create_table('store_game', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True)),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('publisher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Publisher'])),
        ))
        db.send_create_signal('store', ['Game'])

        # Adding M2M table for field developer on 'Game'
        db.create_table('store_game_developer', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('game', models.ForeignKey(orm['store.game'], null=False)),
            ('developer', models.ForeignKey(orm['store.developer'], null=False))
        ))
        db.create_unique('store_game_developer', ['game_id', 'developer_id'])

        # Adding M2M table for field genres on 'Game'
        db.create_table('store_game_genres', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('game', models.ForeignKey(orm['store.game'], null=False)),
            ('genre', models.ForeignKey(orm['store.genre'], null=False))
        ))
        db.create_unique('store_game_genres', ['game_id', 'genre_id'])

        # Adding model 'Genre'
        db.create_table('store_genre', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
        ))
        db.send_create_signal('store', ['Genre'])

        # Adding model 'Featured'
        db.create_table('store_featured', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('platform', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('game', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['store.Game'], unique=True)),
        ))
        db.send_create_signal('store', ['Featured'])

        # Adding model 'Giveaway'
        db.create_table('store_giveaway', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Game'])),
            ('percentOff', self.gf('django.db.models.fields.IntegerField')()),
            ('startTime', self.gf('django.db.models.fields.DateTimeField')()),
            ('endTime', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('store', ['Giveaway'])

        # Adding model 'GameMedia'
        db.create_table('store_gamemedia', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('content', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('mediaType', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.MediaType'])),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Game'])),
        ))
        db.send_create_signal('store', ['GameMedia'])

        # Adding model 'MediaType'
        db.create_table('store_mediatype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('template', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal('store', ['MediaType'])

        # Adding model 'GameForum'
        db.create_table('store_gameforum', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('game', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['store.Game'], unique=True)),
        ))
        db.send_create_signal('store', ['GameForum'])

        # Adding M2M table for field administrators on 'GameForum'
        db.create_table('store_gameforum_administrators', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('gameforum', models.ForeignKey(orm['store.gameforum'], null=False)),
            ('storeuser', models.ForeignKey(orm['store.storeuser'], null=False))
        ))
        db.create_unique('store_gameforum_administrators', ['gameforum_id', 'storeuser_id'])

        # Adding model 'Category'
        db.create_table('store_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('forum', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.GameForum'])),
        ))
        db.send_create_signal('store', ['Category'])

        # Adding model 'Thread'
        db.create_table('store_thread', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Category'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Storeuser'])),
        ))
        db.send_create_signal('store', ['Thread'])

        # Adding model 'Post'
        db.create_table('store_post', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content', self.gf('django.db.models.fields.CharField')(max_length=2000, null=True)),
            ('timePosted', self.gf('django.db.models.fields.DateTimeField')()),
            ('thread', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Thread'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Storeuser'])),
        ))
        db.send_create_signal('store', ['Post'])

        # Adding model 'Storeuser'
        db.create_table('store_storeuser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
        ))
        db.send_create_signal('store', ['Storeuser'])

        # Adding M2M table for field gamesOwned on 'Storeuser'
        db.create_table('store_storeuser_gamesOwned', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('storeuser', models.ForeignKey(orm['store.storeuser'], null=False)),
            ('game', models.ForeignKey(orm['store.game'], null=False))
        ))
        db.create_unique('store_storeuser_gamesOwned', ['storeuser_id', 'game_id'])


    def backwards(self, orm):
        # Deleting model 'Publisher'
        db.delete_table('store_publisher')

        # Deleting model 'Developer'
        db.delete_table('store_developer')

        # Deleting model 'Game'
        db.delete_table('store_game')

        # Removing M2M table for field developer on 'Game'
        db.delete_table('store_game_developer')

        # Removing M2M table for field genres on 'Game'
        db.delete_table('store_game_genres')

        # Deleting model 'Genre'
        db.delete_table('store_genre')

        # Deleting model 'Featured'
        db.delete_table('store_featured')

        # Deleting model 'Giveaway'
        db.delete_table('store_giveaway')

        # Deleting model 'GameMedia'
        db.delete_table('store_gamemedia')

        # Deleting model 'MediaType'
        db.delete_table('store_mediatype')

        # Deleting model 'GameForum'
        db.delete_table('store_gameforum')

        # Removing M2M table for field administrators on 'GameForum'
        db.delete_table('store_gameforum_administrators')

        # Deleting model 'Category'
        db.delete_table('store_category')

        # Deleting model 'Thread'
        db.delete_table('store_thread')

        # Deleting model 'Post'
        db.delete_table('store_post')

        # Deleting model 'Storeuser'
        db.delete_table('store_storeuser')

        # Removing M2M table for field gamesOwned on 'Storeuser'
        db.delete_table('store_storeuser_gamesOwned')


    models = {
        'store.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'forum': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.GameForum']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.developer': {
            'Meta': {'object_name': 'Developer'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.featured': {
            'Meta': {'object_name': 'Featured'},
            'game': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['store.Game']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'store.game': {
            'Meta': {'object_name': 'Game'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'developer': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['store.Developer']", 'null': 'True', 'symmetrical': 'False'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['store.Genre']", 'null': 'True', 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'publisher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Publisher']"})
        },
        'store.gameforum': {
            'Meta': {'object_name': 'GameForum'},
            'administrators': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['store.Storeuser']", 'null': 'True', 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'game': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['store.Game']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'store.gamemedia': {
            'Meta': {'object_name': 'GameMedia'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Game']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mediaType': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'})
        },
        'store.giveaway': {
            'Meta': {'object_name': 'Giveaway'},
            'endTime': ('django.db.models.fields.DateTimeField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Game']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'percentOff': ('django.db.models.fields.IntegerField', [], {}),
            'startTime': ('django.db.models.fields.DateTimeField', [], {})
        },
        'store.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'template': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        'store.post': {
            'Meta': {'object_name': 'Post'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'thread': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Thread']"}),
            'timePosted': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Storeuser']"})
        },
        'store.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.storeuser': {
            'Meta': {'object_name': 'Storeuser'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'gamesOwned': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['store.Game']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'})
        },
        'store.thread': {
            'Meta': {'object_name': 'Thread'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Category']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Storeuser']"})
        }
    }

    complete_apps = ['store']