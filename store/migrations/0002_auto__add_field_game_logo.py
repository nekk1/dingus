# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Game.logo'
        db.add_column('store_game', 'logo',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Game.logo'
        db.delete_column('store_game', 'logo')


    models = {
        'store.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'forum': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.GameForum']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.developer': {
            'Meta': {'object_name': 'Developer'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.featured': {
            'Meta': {'object_name': 'Featured'},
            'game': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['store.Game']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'store.game': {
            'Meta': {'object_name': 'Game'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'developer': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['store.Developer']", 'null': 'True', 'symmetrical': 'False'}),
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['store.Genre']", 'null': 'True', 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'publisher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Publisher']"})
        },
        'store.gameforum': {
            'Meta': {'object_name': 'GameForum'},
            'administrators': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['store.Storeuser']", 'null': 'True', 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'game': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['store.Game']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'store.gamemedia': {
            'Meta': {'object_name': 'GameMedia'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Game']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mediaType': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.MediaType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'})
        },
        'store.giveaway': {
            'Meta': {'object_name': 'Giveaway'},
            'endTime': ('django.db.models.fields.DateTimeField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Game']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'percentOff': ('django.db.models.fields.IntegerField', [], {}),
            'startTime': ('django.db.models.fields.DateTimeField', [], {})
        },
        'store.mediatype': {
            'Meta': {'object_name': 'MediaType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'template': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        'store.post': {
            'Meta': {'object_name': 'Post'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'thread': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Thread']"}),
            'timePosted': ('django.db.models.fields.DateTimeField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Storeuser']"})
        },
        'store.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        'store.storeuser': {
            'Meta': {'object_name': 'Storeuser'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'gamesOwned': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['store.Game']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'})
        },
        'store.thread': {
            'Meta': {'object_name': 'Thread'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Category']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Storeuser']"})
        }
    }

    complete_apps = ['store']