from django.db import models
from django.db.models import Avg
from django.utils import timezone
import store.templatetags.kwacros
import datetime
import math

#Games models
class Publisher(models.Model):
	name = models.CharField(max_length = 100)
	logo = models.FileField(upload_to='pub_logos')
	description = models.TextField()
	def __unicode__(self):
		return self.name

class Developer(models.Model):
	name = models.CharField(max_length = 100)
	logo = models.FileField(upload_to='dev_logos')
	description = models.TextField()
	def __unicode__(self):
		return self.name

class Game(models.Model):

	platform = models.ManyToManyField('Platform')
	name = models.CharField(max_length = 100)
	description = models.TextField()
	logo = models.FileField(upload_to='game_logos')
	price = models.FloatField()
	pub_date = models.DateTimeField('date_published')
	publisher = models.ForeignKey('Publisher')
	developer = models.ManyToManyField('Developer')
	genres = models.ManyToManyField('Genre')

	isOwned = False

	def __unicode__(self):
		return unicode(self.name)
		
	def isNewArrival(self):
		return self.pub_date <= timezone.now() or self.pub_date >= timezone.now() #self.pub_date >= timezone.now() - datetime.timedelta(days=3) and self.pub_date <= timezone.now()
	def isOwnedBy(username):
		for game in User.objects.get(name=username).gamesOwned.all:
			if game.name == self.name:
				return true
		return false
	def getRating(self):
		return GameRating.objects.filter(game=self).aggregate(Avg('rating'))['rating__avg']

class GameComment(models.Model):
	game = models.ForeignKey('Game')
	user = models.ForeignKey('Storeuser')
	timePosted = models.DateTimeField('time_posted', auto_now_add=True)
	content = models.CharField(max_length = 1000)
	def __unicode__(self):
		return unicode(self.user) + " - " + self.content;

class GameRating(models.Model):
	game = models.ForeignKey('Game')
	user = models.ForeignKey('Storeuser')
	rating = models.IntegerField()

	def __unicode__(self):
		return game.name + " - " + str(rating) + ", " + user.username
		
class Platform(models.Model):
	
	name = models.CharField(max_length = 20)
	
	def __unicode__(self):
		return self.name

class Genre(models.Model):
	name = models.CharField(max_length = 50)
	def __unicode__(self):
		return self.name


class Featured(models.Model):
	game = models.OneToOneField('Game')
	def __unicode__(self):
		return self.game.name;

class Giveaway(models.Model):
	game = models.ForeignKey('Game')
	percentOff = models.IntegerField()
	startTime = models.DateTimeField('start_time')
	endTime = models.DateTimeField('end_time')
	def __unicode__(self):
		return self.game.name + ": -"+str(self.percentOff)+"% off"

class GameMedia(models.Model):
	name = models.CharField(max_length = 100)
	content = models.CharField(max_length = 200)
	mediaType = models.ForeignKey('MediaType')
	game = models.ForeignKey('Game')
	def __unicode__(self):
		return unicode(self.mediaType)+": "+self.name

class MediaType(models.Model):
	name = models.CharField(max_length = 100)
	template = models.TextField()
	def __unicode__(self):
		return self.name

#Forum models
class GameForum(models.Model):
	description = models.CharField(max_length = 200)
	game = models.OneToOneField(Game)
	administrators = models.ManyToManyField('Storeuser')
	def __unicode__(self):
		return unicode(self.game)+": "+unicode(self.description)

class Category(models.Model):
	name = models.CharField(max_length = 100)
	description = models.CharField(max_length = 200)
	forum = models.ForeignKey('GameForum')
	def __unicode__(self):
		return self.name

class Thread(models.Model):
	name = models.CharField(max_length = 100)
	description = models.CharField(max_length = 200)
	category = models.ForeignKey('Category')
	user = models.ForeignKey('Storeuser')
	def __unicode__(self):
		return self.name
	def numPosts(self):
		return Post.objects.filter(thread=self).count()
	def numPages(self):
		return int(math.ceil((self.numPosts()-1)/10))

class Post(models.Model):
	content = models.CharField(max_length = 2000)
	timePosted = models.DateTimeField('time_posted', auto_now_add=True)
	thread = models.ForeignKey('Thread')
	user = models.ForeignKey('Storeuser')
	def __unicode__(self):
		return unicode(self.user) + ":" + self.content;

#Users models
class Storeuser(models.Model):
	username = models.CharField(max_length = 20)
	password = models.CharField(max_length = 50)
	email = models.CharField(max_length = 50)
	gamesOwned = models.ManyToManyField('Game', blank=True)
	credit = models.FloatField()

	# def Owns(gamename):
	# 	for (game in self.gamesOwned.all):
	# 		if (game.name == gamename):
	# 			return true
	# 	return false

	def getComments(self):
		return GameComment.objects.filter(user=self)

	def __unicode__(self):
		return self.username;