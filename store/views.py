# Create your views here.
from django.core.servers.basehttp import FileWrapper
from django.http import HttpResponse
from django.template import Context, RequestContext, loader
from django.db import connection
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Count
from collections import Counter
import logging
import math
import hashlib
from store.models import *
import random
import os
import mimetypes, tempfile, zipfile

def index(request):
	cursor = connection.cursor()
	cursor.execute(
	"""SELECT GAME_ID FROM
		(
		SELECT GAME_ID, COUNT(GAME_ID) cnt, AVG(RATING) rating FROM STORE_GAMERATING
		GROUP BY GAME_ID
                ORDER BY cnt*rating DESC
		)
		WHERE ROWNUM = 1
	""")

	row = cursor.fetchone()
	toprated =  Game.objects.get(id=row[0])

	cursor.execute(
	"""SELECT gid FROM
		(
		SELECT GAME_ID gid, COUNT(GAME_ID) cnt FROM STORE_STOREUSER_GAMESOWNED
		GROUP BY GAME_ID
		ORDER BY cnt DESC
		)
		WHERE ROWNUM = 1
	""")
	row = cursor.fetchone()
	mostdownloaded=Game.objects.get(id=row[0])

	cursor.execute(
	"""SELECT * FROM
		(
		SELECT id FROM STORE_GAME ORDER BY PUB_DATE DESC
		)
		WHERE ROWNUM = 1
	""")
	row = cursor.fetchone()
	newarrival=Game.objects.get(id=row[0])

	template = loader.get_template('index.jade')
	linuxgames = Game.objects.filter(platform__name="Linux")
	wingames = Game.objects.filter(platform__name="Windows")
	macosgames = Game.objects.filter(platform__name="Mac OS")

	featured = Featured.objects.values_list('game__id', flat=True)
	featured = Game.objects.filter(id__in=featured)

	context = ({
		"wingames": wingames,
		"linuxgames": linuxgames,
		"macosgames": macosgames,
		"featured": featured,
		"toprated": toprated,
		"newarrival": newarrival,
		"mostdownloaded": mostdownloaded
	})

	return HttpResponse(template.render(RequestContext(request, context)))


def game(request, game_name):
	template = loader.get_template('game.jade')

	tags = Game.objects.get(name=game_name)
	tags = tags.genres.all()

	similar = set(Game.objects.filter(genres__id__in=tags).exclude(name=game_name)[:4])

	linuxgames = Game.objects.filter(platform__name="Linux")
	wingames = Game.objects.filter(platform__name="Windows")
	macosgames = Game.objects.filter(platform__name="Mac OS")

	game = get_object_or_404(Game, name=game_name)
	comments = GameComment.objects.filter(game=game)

	canRate = False
	username = None

	try:
		username = request.session["username"]
	except:
		username = None

	
	if (username != None):
		print username
		user = Storeuser.objects.get(username=username)
		hasRating = GameRating.objects.filter(game=game.id, user=user.id)
		hasGame = user.gamesOwned.filter(id=game.id)

		if (len(hasGame) > 0):
			game.isOwned = True
		else:
			game.isOwned = False

		#print hasRating
		if (len(hasRating) > 0 and game.isOwned):
			canRate = False
		else:
			canRate = True

	inCart = False
	cartGames = []
	try:
		cartGames = request.session['cartGames']
	except:
		cartGames = []

	if (game in cartGames):
		inCart = True
	
	context = ({
		"game": game,
		"comments": comments,
		"wingames": wingames,
		"linuxgames": linuxgames,
		"macosgames": macosgames,
		"similar": similar,
		"canRate": canRate,
		"inCart": inCart
	})


	return HttpResponse(template.render(RequestContext(request, context)))

def addComment(request, game_name):
	game = get_object_or_404(Game, name=game_name)

	content = request.POST['content']
	if( not content or not game ):
		return redirect('store_index')

	user = Storeuser.objects.get(id=request.session['userId'])

	comment = GameComment(game=game, user = user, content = content)
	comment.save()
	return redirect('game', game_name)

def addRating(request, game_name):
	game = get_object_or_404(Game, name=game_name)

	rating = request.POST['rating']
	if( not rating or not game ):
		return redirect('store_index')

	user = Storeuser.objects.get(id=request.session['userId'])
	print "RATING: " + str(rating)
	rating = GameRating(game=game, user=user, rating=rating)

	rating.save()
	return redirect('game', game_name)

def addCash(request):

	template = loader.get_template('addCredit.jade')

	user = None
	try:
		user = Storeuser.objects.get(id=request.session['userId'])
	except: 
		return redirect('store_index')

	context = Context({ 

		"loggedUser": user

	})
	requestContext = RequestContext(request, context)

	try:
		strAmount = request.POST['amount']
		print strAmount + " cash"
		cashAmount = float(strAmount)

		user.credit += cashAmount
		user.save()
	except:
		None

	return HttpResponse(template.render(requestContext))

def cartRedirect(request, game_name):

	return redirect('addToCart', game_name)

def addToCart(request, game_name):

	print "GAME NAME: " + game_name

	game = Game.objects.get(name=game_name)

	cartGames = []
	try:
		cartGames = request.session['cartGames']
	except:
		cartGames = []

	if (game not in cartGames):
		cartGames.append(game)
		request.session['cartGames'] = cartGames
	else:
		print "Already in here badman"

	for game in cartGames:
		print "GAME:" + game.name


	return redirect("/game/" + game_name)

def removeFromCart(request, game_name):

	game = Game.objects.get(name=game_name)

	cartGames = []
	try:
		cartGames = request.session['cartGames']
	except:
		cartGames = []

	if (game in cartGames):
		cartGames.remove(game)

	for game in cartGames:
		print "Remove list: " + game.name

	request.session['cartGames'] = cartGames

	return redirect("/cart")

def cart(request):

	template = loader.get_template('cart.jade')
	user = None
	cartGames = []

	try:
		user = Storeuser.objects.get(id=request.session['userId'])
	except:
		return redirect('login')

	try:
		cartGames = request.session['cartGames']
	except:
		cartGames = []

	

	totalPrice = 0
	for game in cartGames:
		game.save()
		totalPrice += game.price
	
	moneyLeft = user.credit - totalPrice

	context = Context({

		"cartGames": cartGames,
		"totalPrice": totalPrice,
		"moneyLeft": moneyLeft

	})

	requestContext = RequestContext(request, context)
	
	return HttpResponse(template.render(requestContext))

def clearCart(request):
	cartGames = []
	request.session['cartGames'] = cartGames
	return redirect('cart')

def buyGames(request):

	template = loader.get_template('confirmbuy.jade')

	user = Storeuser.objects.get(id=request.session['userId'])
	print "logged: " + user.username 
	print "credit: " + str(user.credit)
	cartGames = []

	try:
		cartGames = request.session['cartGames']
	except:
		cartGames = []

	totalPrice = 0
	for game in cartGames:
		totalPrice += game.price

	moneyLeft = user.credit - totalPrice

	context = Context({

		"cartGames": cartGames,
		"totalPrice": totalPrice,
		"loggedUser": user,
		"moneyLeft": moneyLeft

	})

	requestContext = RequestContext(request, context)
	return HttpResponse(template.render(requestContext))

def buyGamesConfirm(request):

	print "CONFIRMED!"

	cartGames = request.session['cartGames']
	user = Storeuser.objects.get(id=request.session['userId'])

	totalPrice = 0
	for game in cartGames:
		totalPrice += game.price

	user.credit -= totalPrice
	user.save()

	for game in cartGames:
		user.gamesOwned.add(game)

	cartGames = []
	request.session['cartGames'] = cartGames

	return redirect('/user/' + str(user.id))


def gamesRoot(request):
	template = loader.get_template('games.jade')

	games = Game.objects.all()

	context = Context({

		"games": games

	})

	requestContext = RequestContext(request, context)

	return HttpResponse(template.render(requestContext))

def games(request, game_id):
	return HttpResponse("hi")
	
def gamesNewArrivals(request):

	newArrivals = Game.objects.all().order_by('-pub_date')[:10]
	template = loader.get_template('games.jade')
	context = Context({ 'games': newArrivals })
	
	return HttpResponse(template.render(context))
	
	
def gamesGenre(request, genre):
	
	template = loader.get_template('games.jade')

	genreGet = Genre.objects.filter(name__exact=genre)[:1].get()
	genreGames = Game.objects.filter(genres__id__exact=genreGet.id)
	
	context = Context({	'games': genreGames })
	requestContext = RequestContext(request, context)

	return HttpResponse(template.render(requestContext))
	
def cheapestGames(request):

	onlyPrice = Game.objects.filter(price__gt=0)	
	Game.objects.filter(id__in=onlyPrice).values('price')
	
	template = loader.get_template('games/cheapest.jade')
	context = Context({ 'games': onlyPrice })
	
	return HttpResponse(template.render(context))
	
def gamesByPublisher(request, publisher):
	
	publisher = Publisher.objects.filter(name__exact=publisher)
	publisher = publisher[0]
	logger.error(publisher.name)
	games = Game.objects.filter(publisher__name=publisher.name)
	template = loader.get_template('games/gamesByPublisher.jade')
	
	context = Context({ 'games': games })
	
	return HttpResponse(template.render(context))

def devgames(request):
	template = loader.get_template('games.jade')
	requestContext = RequestContext(request)
	
	return HttpResponse(template.render(requestContext))

def publishergames(request):
	template = loader.get_template('games.jade')
	requestContext = RequestContext(request)
	
	return HttpResponse(template.render(requestContext))

def devs(request):
	template = loader.get_template('devs.jade')
	devs = Developer.objects.all()
	context = Context({'devs':devs})
	requestContext = RequestContext(request, context)
	print devs
	
	return HttpResponse(template.render(requestContext))

def dev(request, dev_name):
	dev = Developer.objects.get(name=dev_name)

	if(not dev):
		return redirect('store_index')

	wingames = []
	linuxgames = []
	macosgames = []
	popular_games = []

	cursor = connection.cursor()
	cursor.execute(
	"""Select Count(*), dev_games.game_id
		FROM(
			SELECT game_id
			from STORE_GAME_DEVELOPER
			where developer_id = %s
		) dev_games
		left join STORE_STOREUSER_GAMESOWNED owned on owned.game_id = dev_games.game_id
		group by dev_games.game_id
	""", [dev.id])

	total_rows = cursor.fetchall()

	for row in total_rows:
		popular_games.append(Game.objects.get(id=row[1]))

	linuxgames = dev.game_set.filter(platform__name="Linux")
	wingames = dev.game_set.filter(platform__name="Windows")
	macosgames = dev.game_set.filter(platform__name="Mac OS")

	template = loader.get_template('publisher.jade')

	context = Context({

		"dev": dev,
		"wingames": wingames,
		"linuxgames": linuxgames,
		"macosgames": macosgames,
		"populargames": popular_games

	})

	requestContext = RequestContext(request, context)

	template = loader.get_template('dev.jade')
	
	return HttpResponse(template.render(requestContext))

def publishers(request):
	template = loader.get_template('publishers.jade')

	context = Context({

		"publishers": Publisher.objects.all()

	})

	requestContext = RequestContext(request, context)
	
	return HttpResponse(template.render(requestContext))

def publisher(request, pub_name):

	pub = Publisher.objects.filter(name__exact=pub_name)


	wingames = []
	linuxgames = []
	macosgames = []
	popular_games = []

	if not pub:
		return redirect("store_index")

	pub = pub[0]

	cursor = connection.cursor()
	cursor.execute(
	"""Select Count(*), pub_games.name
		FROM(
			SELECT game.name, game.id
			from Store_game game
			left join store_publisher pub on game.publisher_id = pub.id
			where pub.name = %s
		) pub_games
		left join STORE_STOREUSER_GAMESOWNED owned on owned.game_id = pub_games.id
		group by pub_games.name
	""", [pub_name])

	total_rows = cursor.fetchall()

	for row in total_rows:
		popular_games.append(Game.objects.get(name=row[1]))

	linuxgames = Game.objects.filter(publisher__id=pub.id, platform__name="Linux")
	wingames = Game.objects.filter(publisher__id=pub.id, platform__name="Windows")
	macosgames = Game.objects.filter(publisher__id=pub.id, platform__name="Mac OS")

	template = loader.get_template('publisher.jade')

	context = Context({

		"publisher": pub,
		"wingames": wingames,
		"linuxgames": linuxgames,
		"macosgames": macosgames,
		"populargames": popular_games

	})

	requestContext = RequestContext(request, context)
	
	return HttpResponse(template.render(requestContext))

def downloadGame(request, game_name):

	game = Game.objects.get(name=game_name)

	logo = game.logo

	filename = logo.file.name.split('/')[-1]
	response = HttpResponse(logo.file, content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename=%s' % game.name

	return response


#=============================================
#
#   Forums
#=============================================

def forum(request, game_name):
	game = Game.objects.get(name=game_name)

	if(not game):
		return redirect('store_index')

	forum = GameForum.objects.get(game=game)
	categories = Category.objects.filter(forum=forum)
	isUserAdmin = forum.administrators.filter(pk=request.session['userId']).count()

	template = loader.get_template('forum.jade')
	context = Context({'game': game, 'forum': forum, 'categories': categories, 'isAdmin': isUserAdmin})

	requestContext = RequestContext(request, context)
	
	return HttpResponse(template.render(requestContext))

def forumCategory(request, game_name, category):

	category = Category.objects.get(name=category, forum__game__name=game_name)
	forum = category.forum
	game = forum.game
	isUserAdmin = category.forum.administrators.filter(pk=request.session['userId']).count()
	if(not category):
		return redirect('store_index')

	threads = Thread.objects.filter(category=category)

	context = Context({
		'game': game,
		'forum': forum,
		'category': category,
		'threads': threads,
		'isAdmin': isUserAdmin
		})

	template = loader.get_template('category.jade')
	requestContext = RequestContext(request, context)
	
	return HttpResponse(template.render(requestContext))

def createThread(request, category_id):
	category = Category.objects.get(pk=category_id)
	user = Storeuser.objects.get(pk=request.session['userId'])
	title = request.POST['title']
	content = request.POST['content']

	if(user and category and title and content):
		thread = Thread(name=title, description=content, user=user, category=category)
		thread.save()
		post = Post(content=content, thread=thread, user=user)
		post.save()

	return redirect('thread', thread.id, 0)


def deleteThread(request, thread_id):
	thread = Thread.objects.get(pk=thread_id)
	isUserAdmin = thread.category.forum.administrators.filter(pk=request.session['userId']).count()

	if(isUserAdmin):
		thread.delete();

	return redirect('forumCategory', thread.category.forum.game.name, thread.category.name)

def createCategory(request, forum_id):
	forum = GameForum.objects.get(id=forum_id)
	isUserAdmin = forum.administrators.filter(pk=request.session['userId']).count()
	title = request.POST['title']
	content = request.POST['content']

	if(forum and isUserAdmin and title and content):
		category = Category(name=title, forum=forum, description=content)
		category.save()
		return redirect('forumCategory', category.forum.game.name, category.name)
	return redirect('forum', forum.game.name)


def deleteCategory(request, category_id):
	category = Category.objects.get(pk=category_id)
	isUserAdmin = category.forum.administrators.filter(pk=request.session['userId']).count()

	if(isUserAdmin):
		category.delete();

	return redirect('forum', category.forum.game.name)



def thread(request, thread_id, page=0):
	thread = Thread.objects.get(id=thread_id)
	numPages = thread.numPages()
	page = int(page)
	posts = Post.objects.filter(thread=thread).order_by('timePosted')[page*10:(page+1)*10]
	isUserAdmin = thread.category.forum.administrators.filter(pk=request.session['userId']).count()

	context = Context({
		'thread': thread,
		'posts': posts,
		'page': page,
		'numPages': numPages,
		'isAdmin': isUserAdmin,
		'nextpage': page+1,
		'prevpage': page-1
		})

	requestContext = RequestContext(request, context)
	template = loader.get_template('thread.jade')
	
	return HttpResponse(template.render(requestContext))


def createPost(request, thread_id):

	content = request.POST['content']
	userId = request.session['userId']

	thread = Thread.objects.get(pk=thread_id)
	user = Storeuser.objects.get(pk=userId)

	post = Post(content=content, thread=thread, user=user)
	post.save()

	return redirect('thread', thread_id, thread.numPages())

def deletePost(request, post_id):
	post = Post.objects.get(pk=post_id)

	thread = post.thread
	user = post.user

	if(user.id == request.session['userId']):
		post.delete();

	return redirect('thread', thread.id, thread.numPages())

#=============================================
#
#   Users
#=============================================

def login(request):
	template = loader.get_template('login.jade')
	requestContext = RequestContext(request)
	
	return HttpResponse(template.render(requestContext))

def logout(request):
	try:
		del request.session['userId']
		del request.session['username']
	except KeyError:
		pass
	return redirect('store_index')

def verify(request):

	username = request.POST['username']
	password = request.POST['password']

	user = None
	if(username and password):
		password = hashlib.sha1(password).hexdigest()
		try:
			user = Storeuser.objects.get(username=username, password=password)
		except:
			return redirect('store.views.login')

	request.session['userId'] = user.id
	request.session['username'] = user.username
	return redirect('store_index')
	
def user(request, user_id):
	template = loader.get_template('user.jade')
	storeuser = Storeuser.objects.get(id=user_id)
	comments = storeuser.getComments()

	context = Context({

		"storeuser": storeuser,
		"comments": comments

	})

	requestContext = RequestContext(request, context)
	return HttpResponse(template.render(requestContext))



#=============================================
#
#   Forums
#=============================================
def stats(request):
	template = loader.get_template('stats.jade')


	cursor = connection.cursor()
	cursor.execute(
	"""
		SELECT NAME FROM
		(
		SELECT GAME_ID, COUNT(GAME_ID) cnt, AVG(RATING) rating FROM STORE_GAMERATING
		GROUP BY GAME_ID
		ORDER BY rating DESC
		) ratings
		LEFT JOIN STORE_GAME
		ON ratings.GAME_ID = STORE_GAME.ID
		WHERE ROWNUM <= 10
	""")
	rows = cursor.fetchall()
	gamenamesraw = []
	for row in rows:
		gamenamesraw.append(row[0])
	tops = [0]
	tops.extend(gamenamesraw)


	cursor.execute(
		"""
			SELECT NAME FROM
			(
			SELECT NAME, COUNT(NAME) cnt FROM STORE_STOREUSER_GAMESOWNED
			LEFT JOIN STORE_GAME
			ON STORE_GAME.ID = STORE_STOREUSER_GAMESOWNED.GAME_ID
			GROUP BY NAME
	                ORDER BY cnt DESC
			) bought
			WHERE ROWNUM <= 10
		""")
	rows = cursor.fetchall()
	gamenamesraw = []
	for row in rows:
		gamenamesraw.append(row[0])
	boughtgames = [0]
	boughtgames.extend(gamenamesraw)



	cursor.execute(
		"""SELECT USERNAME FROM
			(
			SELECT USERNAME, COUNT(USERNAME) cnt FROM STORE_STOREUSER_GAMESOWNED
			LEFT JOIN STORE_STOREUSER
			ON STORE_STOREUSER.ID = STORE_STOREUSER_GAMESOWNED.STOREUSER_ID
			GROUP BY USERNAME
	                ORDER BY cnt DESC
			) bought
			WHERE ROWNUM <= 10
		""")
	rows = cursor.fetchall()
	usernamesraw = []
	for row in rows:
		usernamesraw.append(row[0])
	topusers = [0]
	topusers.extend(usernamesraw)



	
	recent = Game.objects.order_by('-pub_date')[:10]
	mostrecent = [0]
	mostrecent.extend(recent)



	cursor.execute(
		"""
			SELECT NAME FROM
			(
			SELECT NAME, COUNT(NAME) cnt FROM STORE_POST
			LEFT JOIN STORE_THREAD
			ON STORE_POST.THREAD_ID = STORE_THREAD.ID
			LEFT JOIN STORE_CATEGORY
			ON STORE_THREAD.CATEGORY_ID = STORE_CATEGORY.ID
			LEFT JOIN STORE_GAMEFORUM
			ON STORE_CATEGORY.FORUM_ID = STORE_GAMEFORUM.ID
			LEFT JOIN STORE_GAME
			ON STORE_GAMEFORUM.GAME_ID = STORE_GAME.ID
			GROUP BY STORE_GAME.NAME
			ORDER BY cnt DESC
			)
			WHERE ROWNUM <=10

		""")
	rows = cursor.fetchall()
	gamenamesraw = []
	for row in rows:
		gamenamesraw.append(row[0])
	activegames = [0]
	activegames.extend(gamenamesraw)





	cursor.execute(
		"""
			SELECT USERNAME FROM
			(
			SELECT STORE_POST.USER_ID, COUNT(STORE_POST.USER_ID) cnt FROM STORE_POST
			GROUP BY STORE_POST.USER_ID
			ORDER BY cnt DESC
			)
			LEFT JOIN STORE_STOREUSER 
			ON STORE_STOREUSER.id = USER_ID
			WHERE ROWNUM <=10
		""")
	rows = cursor.fetchall()
	usernames = []
	for row in rows:
		usernames.append(row[0])
	mostactiveusers = [0]
	mostactiveusers.extend(usernames)
	
	

	context = Context({'toprated': tops, 'mostbought': boughtgames, 'mostrecent': mostrecent, 'mostactive': activegames, 'mostactiveusers': mostactiveusers, 'topusers': topusers})

	requestContext = RequestContext(request, context)
	return HttpResponse(template.render(requestContext))


	# cursor = connection.cursor()
	# cursor.execute(
	# """SELECT GAME_ID FROM
	# 	(
	# 	SELECT GAME_ID, COUNT(GAME_ID) cnt, AVG(RATING) rating FROM STORE_GAMERATING
	# 	GROUP BY GAME_ID
 #                ORDER BY cnt*rating DESC
	# 	)
	# 	WHERE ROWNUM = 1
	# """)

	# row = cursor.fetchone()
